export const API = "https://jsonplaceholder.typicode.com/"
export const USER_API = API + "users"
export const POSTS_API = API + "posts"
