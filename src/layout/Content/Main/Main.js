import React from 'react'
import PostBox from '../../../components/PostBox/PostBox'
import PostList from '../../../components/PostList'

function Main() {
  return (
    <div className='w-75'>
        <PostList/>
        <PostBox/>
    </div>
  )
}

export default Main