import React from 'react'
import Main from './Main'
import SideBar from './SideBar'
import "./Content.css"

function Content() {
  return (
    <div className='d-flex p-2 mw-100 content-container'>
        <SideBar/>
        <Main/>
    </div>
  )
}

export default Content