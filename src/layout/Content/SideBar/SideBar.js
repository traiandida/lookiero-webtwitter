import React from 'react'
import UserList from '../../../components/UserList'

function SideBar() {

  return (
    <div className='w-25'>
        <UserList/>
    </div>
  )
}

export default SideBar