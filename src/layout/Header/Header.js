import React from 'react'
import logo from '../../assets/lookiero-logo.png'
import "./Header.css"

function Header() {
  return (
    <header>
        <img src={logo} alt="Lookiero logo"/>
        <h1>Lookiero - Social Networking</h1>
    </header>
  )
}

export default Header