import { render, screen } from "@testing-library/react";
import React from "react";
import Header from "./Header";

test('loads and displays header', () => {
    render(<Header/>)
    expect(screen.getByRole('heading')).toHaveTextContent("Lookiero - Social Networking")
    expect(screen.getByAltText('Lookiero logo')).toBeInTheDocument()
})