import React from 'react'
import { createRoot } from 'react-dom/client'
import App from './App'
import Root from './store'


const container = document.getElementById('root')
const root = createRoot(container)
root.render(
    <Root>
        <App/>        
    </Root>
)