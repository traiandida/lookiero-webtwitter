import { fireEvent, render, screen } from "@testing-library/react"
import UserCard from "./UserCard"

const user = {
    name: 'Test user',
    follow: true        
}
const handleClick = jest.fn()

const setup = () => render(<UserCard user={user} handler={handleClick}/>)

test("reders userCard", () => {
    setup()
    expect(screen.getByText(user.name)).toBeInTheDocument()
    expect(screen.getByRole('button')).toBeInTheDocument()
})

test("button click", () => {
    setup();
    fireEvent.click(screen.getByRole('button'))
    expect(handleClick).toHaveBeenCalledTimes(1);
})