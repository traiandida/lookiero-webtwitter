import React from 'react'
import { Button } from 'react-bootstrap';

function UserCard({handler,user}) {
  
  const btnVariant = user.follow ? 'secondary' : 'primary'
  const btnTxt = user.follow ? 'Unfollow' : 'Follow'

 

  return (
    <div className="card">
      <div className="card-body">
        <p className="card-text">{user.name}<Button className='m-2' variant={btnVariant} style={{float: 'right'}} onClick={() => handler(user.id)}>{btnTxt}</Button></p>
      </div>
    </div>
  )
}

export default UserCard