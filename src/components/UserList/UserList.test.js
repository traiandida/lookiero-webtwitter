import { render, screen } from "@testing-library/react"
import Root from "../../store"
import UserList from "./UserList"

const setup = () => render(<Root><UserList/></Root>)

test("renders UserList", () => {
    setup()
    expect(screen.getByRole('heading')).toHaveTextContent("Users")
})