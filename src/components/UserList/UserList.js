import React, { useState , useEffect} from 'react'
import UserCard from './UserCard'
import axios from 'axios'
import "./UserList.css"
import { USER_API } from '../../constants'
import { PushFollowersToFront } from '../../util/PushFollowersToFront'
import { useDispatch } from 'react-redux'
import { followUserHandler, storeUsers } from '../../store/actions'


function UserList() {

  const [users, setUsers] = useState([])
  const dispatch = useDispatch()
    
  useEffect(() => {
      
      axios.get(USER_API).then(res => {
        const data = res.data
        
        // To test followers ui
        data[2].follow = true;
        data[5].follow = true;
        data[6].follow = true;
        
        // To move followers to top positions
        setUsers(PushFollowersToFront(data))  

        //Set a local storage item with the name to avoid making a request
        //for the posts
        data.forEach(user => {
          if(user.follow) {
            dispatch(followUserHandler(user.id))
          }
          localStorage.setItem(user.id, user.name)}
        )
        dispatch(storeUsers(data))

      })
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleFollowButton = (id) => {

    const newState = [...users]
    const idx = newState.findIndex((user) => user.id === id)
    newState[idx].follow = !newState[idx].follow
    
    setUsers(PushFollowersToFront(newState))
    dispatch(followUserHandler(id))
    
  }
  
  const renderUsers = users.map((user) => {
    return (
      <UserCard 
        key={user.id} 
        user={user} 
        handler={handleFollowButton}
      />
    )
  })
  
  return (
    <div className='user-list-container border'>
        <h4>Users</h4>
        <div className='user-list overflow-auto'>
            {renderUsers}
        </div>
    </div>
  )
}

export default UserList