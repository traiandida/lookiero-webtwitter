import React from 'react'
import { timeFormatter } from '../../../util/TimeFormatter'

function Post({post}) {
    
  const userName = post.userId === 99 ? 'Test user' : localStorage.getItem(post.userId)
  const date = timeFormatter(post.date)

  return (
    <div className='row border position-relative'>
        <h5>{userName}</h5>
        <small>{post.body}</small>
        <span className='position-absolute small' style={{right:"0px", width:"100px"}}>
            {date}
        </span>
    </div>
  )
}

export default Post