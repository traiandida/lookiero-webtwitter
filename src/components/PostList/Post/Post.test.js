import { render, screen } from "@testing-library/react"
import Post from "./Post"

test("reders post", () => {
    const post = {
        userId: 99,
        date: new Date(),
        body: "TEST POST"
    }
    render(<Post post={post}/>)
    expect(screen.getByRole('heading')).toHaveTextContent('Test user')
    expect(screen.getByText(post.body)).toBeInTheDocument()

})