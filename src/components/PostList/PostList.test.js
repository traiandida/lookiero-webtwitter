import { render, screen } from "@testing-library/react"
import Root from "../../store"
import PostList from "./PostList"

const setup = () => render(<Root><PostList/></Root>)

test("renders PostList", () => {
    setup()
    expect(screen.getByRole('heading')).toHaveTextContent("Timeline")
})
