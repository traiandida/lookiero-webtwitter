import axios from 'axios'
import React, { useEffect, useState } from 'react'
import Post from './Post'
import { POSTS_API } from '../../constants'
import "./PostList.css"
import { randomDate } from '../../util/RandomDate'
import { useDispatch, useSelector } from 'react-redux'
import { storePosts } from '../../store/actions'

function PostList() {

  
  const followers = useSelector((state) => state.followers)
  const posts = useSelector((state) => state.posts)
  const [followedPosts, setFollowedPosts] = useState([]);
  
  const dispatch = useDispatch()
  
  useEffect(() => {

    // if(posts.length === 0) {
    if(posts.length === 0) {
      axios.get(POSTS_API).then(res => {
        const data = res.data
  
        //This api doesn't return dates for each post
        //I created a small util to generate random dates
        data.forEach(post => {
          post.date = randomDate();
        });
        
        // Sort by date
        data.sort((a,b) => new Date(b.date) - new Date(a.date))

        dispatch(storePosts(data))         
      })
      
    }    
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if(followers.length > 0) {
      setFollowedPosts(posts.filter((post) => followers.includes(post.userId) || post.userId === 99 ))
    }else{
      setFollowedPosts(posts)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [followers, posts])

  const renderPosts = followedPosts.map((post) => {
    return(
      <Post
        key={post.id}
        post={post}
      />
    )
  })

  return (
    <div className='post-list-container border'>
        <h4>Timeline</h4>
        <div className='post-list container overflow-auto'>
          {renderPosts}
        </div>
    </div>
  )
}

export default PostList