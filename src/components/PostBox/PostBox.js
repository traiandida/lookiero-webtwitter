
import React, { useState , useRef } from 'react'
import { Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { addPost } from '../../store/actions'
import { v4 as uuid } from 'uuid';
import './PostBox.css'

function PostBox() {

  const [newPost, setNewPost] = useState("")
  const textAreaRef = useRef(null)
  const dispatch = useDispatch()

  const handleButton = () => {
    if(newPost === "") return;
    
    const post = {
      id : uuid(), 
      userId: 99,
      date: new Date(),
      body: newPost
    }

    dispatch(addPost(post))

    textAreaRef.current.value = '';
  }

  return (
    <div className='post-box-container'>
        <h4>Post a new message</h4>
        <textarea placeholder='Post a new message' className='border' onChange={ev => setNewPost(ev.target.value)} ref={textAreaRef} maxLength="280"></textarea>
        <Button className='m-2' variant='primary' style={{float: 'right'}} onClick={handleButton}>Post</Button>
    </div>
  )
}

export default PostBox