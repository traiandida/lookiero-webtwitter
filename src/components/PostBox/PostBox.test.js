import { render, screen } from "@testing-library/react"
import PostBox from "./PostBox"
import Root from "../../store"

const setup = () => render(<Root><PostBox/></Root>)

test("renders PostBox test title, textarea and button", () => {
    setup()
    expect(screen.getByRole('heading')).toHaveTextContent("Post a new message")
    expect(screen.getByPlaceholderText('Post a new message')).toBeInTheDocument()
    expect(screen.getByRole('button')).toBeInTheDocument()
})

