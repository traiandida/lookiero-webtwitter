import { PushFollowersToFront } from "../PushFollowersToFront"
import { randomDate } from "../RandomDate"
import { timeFormatter } from "../TimeFormatter"

test('pushes followed users to front of array', () => {
    const arr = [
        {user: 1},
        {user: 2, follow: true},
        {user: 3, follow: false},
        {user: 4, follow: true}
    ]
    const auxArr = PushFollowersToFront(arr)

    expect(auxArr[0].follow).toBeTruthy()
    expect(auxArr[1].follow).toBeTruthy()
    
})

test("util test default year and month", () => {
   const testDate = randomDate() 
   expect(testDate.getFullYear()).toBe(2022)
   expect(testDate.getMonth()).toBeGreaterThanOrEqual(1)
   expect(testDate.getMonth()).toBeLessThan(4)
})

test("util format date", () => {
    const testDateFormat = timeFormatter(new Date())
    expect(testDateFormat).toContain('seconds')

})