export function randomDate(begin = new Date(2022,1,1), end = new Date(2022,3,13)){
    return new Date(begin.getTime() + Math.random() * (end.getTime() - begin.getTime()))
}