export function PushFollowersToFront(array = []){
    return array.sort((a,b) => a.follow === true ? -1 : b.follow === true ? 1 : 0)
}