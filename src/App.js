import React from 'react'
import Header from './layout/Header';
import Content from './layout/Content';

import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {

  return (
    <>
        <Header/>
        <Content/>
    </>
  )
}

export default App