import {combineReducers} from "redux"
import postsReducers from './posts'
import usersReducers from './users'
import followersReducers from "./followers"

export default combineReducers({
    posts: postsReducers,
    users: usersReducers,
    followers: followersReducers
})