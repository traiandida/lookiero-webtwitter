import { ADD_POST, STORE_POSTS } from "../actionTypes";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action){
    switch(action.type){
        case STORE_POSTS:
            return [...state, ...action.payload]
        case ADD_POST:
            return [...state, action.payload]
        default:
            return state;
    }
}