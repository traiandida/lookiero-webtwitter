import { STORE_USERS } from "../../actionTypes"
import usersReducer from '../../reducers/users'

test("action handler of type STORE_POSTS", () => {
    const action = {
        type: STORE_USERS,
        payload: [{userId: 1}, {userId:2}]
    }
    const newState = usersReducer([],action)
    expect(newState.length).toEqual(2)
})

test("action handler of uknown type", () => {
    const newState = usersReducer([],{type: "UNKNOW_TYPE"})
    expect(newState).toEqual([])
})