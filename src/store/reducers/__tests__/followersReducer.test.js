import { FOLLOW_USER } from "../../actionTypes"
import followReducers from '../../reducers/followers'

test("action handler of type FOLLOW_USER", () => {
    const action = {
        type: FOLLOW_USER,
        payload: 1
    }
    let newState = followReducers([],action)
    expect(newState).toEqual([1])

    newState = followReducers([1], action)
    expect(newState).toEqual([])
})

test("action handler of uknown type", () => {
    const newState = followReducers([],{type: "UNKNOW_TYPE"})
    expect(newState).toEqual([])
})