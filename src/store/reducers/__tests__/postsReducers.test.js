import { ADD_POST, STORE_POSTS } from "../../actionTypes"
import postsReducers from '../../reducers/posts'


test("action handler of type STORE_POSTS", () => {
    const action = {
        type: STORE_POSTS,
        payload: [{postId: 1}, {postId:2}]
    }
    const newState = postsReducers([],action)
    expect(newState.length).toEqual(2)
})


test("action handler of type ADD_POST", () => {
    const action = {
        type: ADD_POST,
        payload: [{postId: 2}]
    }
    const newState = postsReducers([{postId: 1}],action)
    expect(newState.length).toEqual(2)
})

test("action handler of uknown type", () => {
    const newState = postsReducers([],{type: "UNKNOW_TYPE"})
    expect(newState).toEqual([])
})