import { STORE_USERS } from "../actionTypes";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action){
    switch(action.type){
        case STORE_USERS:             
            return [...state, ...action.payload]
        default:
            return state
    }
}