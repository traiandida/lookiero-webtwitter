import { FOLLOW_USER } from "../actionTypes";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action){
    switch(action.type){
        case FOLLOW_USER:
            const id = action.payload;
            if(state.includes(id)){
                state.splice(state.indexOf(id),1)
            }else{
                state.push(id);
            }
            return [...state]
        default:
            return state
    }
}