// User types
export const STORE_USERS = 'STORE_USERS'
export const FOLLOW_USER = 'FOLLOW_USER'

// Posts types
export const STORE_POSTS = 'STORE_POSTS'
export const ADD_POST = 'ADD_POST'