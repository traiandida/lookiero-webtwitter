import { followUserHandler, storeUsers , storePosts, addPost } from ".."
import { STORE_USERS, FOLLOW_USER, STORE_POSTS, ADD_POST } from "../../actionTypes"


describe('storeUsers action tests', () => {

    const action = storeUsers([{id:1}, {id:2}])

    test("correct type", () => {
        expect(action.type).toEqual(STORE_USERS);
    })
    test("correct payload", () => {
        expect(action.payload.length).toEqual(2)
    })
})

describe('followUserHandler action tests', () => {

    const action = followUserHandler(8)

    test("correct type", () => {
        expect(action.type).toEqual(FOLLOW_USER);
    })
    test("correct payload", () => {
        expect(action.payload).toBe(8)
    })
})

describe('storePosts action tests', () => {

    const action = storePosts([{id:1}, {id:2}])

    test("correct type", () => {
        expect(action.type).toEqual(STORE_POSTS);
    })
    test("correct payload", () => {
        expect(action.payload.length).toEqual(2)
    })
})

describe('addPost action tests', () => {

    const action = addPost({id:1, post: "TEST"})

    test("correct type", () => {    
        expect(action.type).toEqual(ADD_POST);
    })
    test("correct payload", () => {
        expect(action.payload).toEqual({id:1, post: "TEST"})
    })
})
