import { STORE_USERS, FOLLOW_USER, STORE_POSTS, ADD_POST} from "../actionTypes";


export function storeUsers(users){   
    return {
        type: STORE_USERS,
        payload: users
    }
}

export function followUserHandler(userId){
    return{
        type: FOLLOW_USER,
        payload: userId
    }
}

export function storePosts(posts){
    return {
        type: STORE_POSTS,
        payload:posts
    }
}

export function addPost(post){
    return {
        type: ADD_POST,
        payload:post
    }
}