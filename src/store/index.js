import React from 'react'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import reduxPromise from 'redux-promise'
import reducers from './reducers'
import { composeWithDevTools } from 'redux-devtools-extension';

// eslint-disable-next-line import/no-anonymous-default-export
export default ({children, initialState = {}}) => {
    
    const store = createStore(
        reducers, 
        initialState, 
        composeWithDevTools(applyMiddleware(reduxPromise))
    )

    return (
        <Provider store = {store}>
            {children}
        </Provider>
    )
}

